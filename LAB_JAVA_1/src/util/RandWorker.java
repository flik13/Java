package util;

import com.Zabchuk.model.Worker;

import java.util.Random;

public class RandWorker {
    public static final int MIN_YEAR = 1970;
    private static final Random ran = new Random();

    private static final String workerName[] = {
            "Олег", "Михайло","Євген","Андрій","Ярослав","Олександр","Володимир","Адам","Анатолій","Антон","Віст","Грива","Дибач",
           "Віта","Донна","Емма","Ївга","Зоя","Клара","Кора","Лада","Мотря","Остап"};
    private static final String  workerSurname[] = {
            "Опалько",
           "Очкур","Конопля","Жебрак","Бухтій","Щетина","Сом","Рись","Калюк","Гак","Влах",
            "Бриж","Барон","Тиран" +
            "","Цар","Хруль","Гашенко","Бих","Масич","Лопух","Квак","Хан","Щурик","Нич","Деде","Ваш","Члек","Пиж"};

    private static String getRandomName(){
        String ranName = workerName[ran.nextInt(workerName.length - 1)];
        return  ranName;
    }
    private static String getRandomSurname(){
        String ranSurname = workerSurname[ran.nextInt(workerName.length - 1)];
        return ranSurname;
    }
    private static Integer getRandomYearOfBirth(){
        return ran.nextInt(2016- MIN_YEAR)+ MIN_YEAR;
    }
    public static Worker RandWorker(){
        return new Worker(getRandomName(), getRandomSurname(), getRandomYearOfBirth());
    }
}
