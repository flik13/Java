package util;

import com.Zabchuk.model.Worker;


public class WorkerBuilder {
    private Worker obj_work;

//darkness is coming
    public WorkerBuilder() {
        obj_work = new Worker();
    }

    public WorkerBuilder surname(String surn){
        obj_work.setSurname(surn);
        return this;
    }
    public WorkerBuilder names(String name){
        obj_work.setName(name);
        return this;
    }
    public WorkerBuilder yearOfBirth(Integer year_of_b){
        obj_work.setYear_of_birth(year_of_b);
        return this;
    }
    public WorkerBuilder yearOfEmployment(Integer year_of_e){
        obj_work.setYear_of_employment(year_of_e);
        return this;
    }
    public Worker build(){
        return obj_work;
    }

    public static Worker[] generateWorkers(){
        Worker arrOfWorkers[] = new Worker[10];
        RandWorker rw;
        for(Integer i = 0 ; i<arrOfWorkers.length;i++)
        {
            rw = new RandWorker();
            arrOfWorkers[i] = new WorkerBuilder().surname(rw.getRandSurname()).names(rw.getRandName()).yearOfBirth(rw.getRandYearOfBirth()).build();
        }
        return  arrOfWorkers;
    }
}