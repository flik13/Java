import com.Zabchuk.model.Worker;
import util.WorkerBuilder;

/**
 * Created by Zabchuk on 04.10.16.
 */
public class Main {
    public static void main(String[] args) {
        Worker worker[] = WorkerBuilder.generateWorkers();
        for (Worker o:worker) {
            System.out.println(o.toString());
        }
    }
}
